package plugins.atournay.jep;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginDaemon;
import icy.plugin.interface_.PluginLibrary;
import plugins.atournay.jep.prefs.JepPreferences;
import plugins.atournay.jep.utils.JepUtils;

public class JepPlugin extends Plugin implements PluginLibrary, PluginDaemon {
    private static JepPlugin instance;


    public JepPlugin() {
    }

    public static JepPlugin getInstance() {
        if (instance == null) {
            instance = new JepPlugin();
        }

        return instance;
    }

    @Override
    public void init() {
        if (!JepPreferences.getInstance().getJepPath().isEmpty()) {
            JepUtils.getInstance().setJepPath(JepPreferences.getInstance().getJepPath(), JepPreferences.getInstance().getPythonRoot());
        }
    }

    @Override
    public void run() {
    }

    @Override
    public void stop() {
    }
}
