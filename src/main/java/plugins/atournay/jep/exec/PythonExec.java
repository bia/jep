package plugins.atournay.jep.exec;

import icy.system.IcyExceptionHandler;
import jep.JepConfig;
import jep.JepException;
import jep.SubInterpreter;

import java.io.Closeable;

/**
 * Class that wraps a Python JEP {@see jep.SubInterpreter} instance. An object of this class returns the interpreter that can be used to call Python code.
 * This class also contains some methods that call Python code to carry out different tasks such as installing modules from Python
 *
 * @author Carlos Garcia Lopez de Haro
 * @author Amandine Tournay
 */
public class PythonExec implements Closeable {
    private SubInterpreter interpreter;
    private JepConfig config;

    /**
     * Open a new instance of JEP to consume Python code or scripts
     */
    public PythonExec() {
        config = new JepConfig();
        config.setClassEnquirer(new DefaultClassEnquirer());

        try {
            interpreter = new SubInterpreter(config);
        }
        catch (JepException e) {
            IcyExceptionHandler.showErrorMessage(new JepException(e), true, true);
        }
    }

    /**
     * Opens a new instance of JEP to consume Python code or scripts with a custom configuration
     * @param customConfig Override JEP configuration
     */
    public PythonExec(JepConfig customConfig) {
        config = customConfig;
        interpreter = new SubInterpreter(config);
    }

    /**
     * Installation of Python packages with the <code>pip install</code> command through Python
     *
     * @param packages Name of the packages to install through pip.
     *                 It can be either the package name (i.e. numpy) or a specific version of a package (i.e. numpy==1.23.1) separated by a space
     * @see plugins.atournay.jep.utils.PythonUtils#installPythonPackage(String) to install packages through the terminal
     */
    public void installPythonPackage(String packages) {
        String command = "try:" + System.lineSeparator();
        command += "\tfrom pip import main as pipmain" + System.lineSeparator();
        command += "except:" + System.lineSeparator();
        command += "\tfrom pip._internal.main import main as pipmain" + System.lineSeparator();
        command += "pipmain(['install', '" + packages + "'])" + System.lineSeparator();

        interpreter.exec(command);
    }

    public SubInterpreter getInterpreter() {
        return interpreter;
    }

    public JepConfig getConfig() {
        return config;
    }

    /**
     * Close the current running instance of Python
     * <b>To reopen a new one, instantiate a new PythonExec class</b>
     */
    @Override
    public void close() {
        if (interpreter != null) {
            interpreter.close();
        }
    }
}
