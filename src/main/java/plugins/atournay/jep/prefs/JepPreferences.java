package plugins.atournay.jep.prefs;

import icy.preferences.PluginPreferences;
import icy.preferences.XMLPreferences;
import plugins.atournay.jep.ui.Python3Preferences;

/**
 * Class managing all Python preferences for Icy
 * @see Python3Preferences for UI
 * @author Amandine Tournay
 */
public class JepPreferences {
    private static JepPreferences instance;
    private static final String PREF_ID = "python3";
    private static final String PYTHON_ROOT = "pythonRoot";
    private static final String PYTHON_EXEC = "pythonExec";
    private static final String JEP_PATH = "jepPath";

    /*
     * Preference Node to encapsulate Python 3 settings
     */
    private static XMLPreferences prefPython;

    private JepPreferences() {
        load();
    }

    public static JepPreferences getInstance() {
        if (instance == null) {
            instance = new JepPreferences();
        }

        return instance;
    }

    /**
     * Load preferences for Python 3 in Icy settings
     */
    public void load() {
        prefPython = PluginPreferences.getPreferences().node(PREF_ID);
    }

    /**
     * Retrieve all preferences related to Python
     * @return Preferences from Python Node
     */
    public XMLPreferences getPythonPreferences() {
        return prefPython;
    }

    /**
     * Retrieve from Python Node preferences the Python root path
     * @return Full path to Python home directory
     */
    public String getPythonRoot() {
        return getPythonPreferences().get(PYTHON_ROOT, "");
    }
    /**
     * Set to the Python Node preferences the path to Python
     * @param path Full path to the Python home directory
     */
    public void setPythonRoot(String path) {
        getPythonPreferences().put(PYTHON_ROOT, path);
    }

    /**
     * Retrieve from Python Node preferences the path to the Python executable file
     * @return Full path to Python executable file
     */
    public String getPythonExecPath() {
        return getPythonPreferences().get(PYTHON_EXEC, "");
    }

    /**
     * Set to the Python Node preferences the path to the Python executable file
     * @param path Full path to the Python executable file
     */
    public void setPythonExecPath(String path) {
        getPythonPreferences().put(PYTHON_EXEC, path);
    }

    /**
     * Retrieve from Python Node preferences the path to the JEP file (libjep)
     * @return Full path to JEP library file
     */
    public String getJepPath() {
        return getPythonPreferences().get(JEP_PATH, "");
    }
    /**
     * Set to the Python Node preferences the path to the JEP file
     * @param path Full path to JEP library file (libjep)
     */
    public void setJepPath(String path) {
        getPythonPreferences().put(JEP_PATH, path);
    }
}
