package plugins.atournay.jep.ui;

import icy.gui.component.IcyTextField;
import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import plugins.atournay.jep.prefs.JepPreferences;
import plugins.atournay.jep.utils.JepUtils;
import plugins.atournay.jep.utils.PythonUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to create a new window in Icy to set JEP
 *
 * @author Amandine Tournay
 */
public class JepPreferencesWindow extends IcyFrame implements ActionListener {
    IcyFrame frame = IcyFrame.findIcyFrame(getInternalFrame());
    private JButton customPythonBrowseButton, applyButton, saveButton, cancelButton;
    private JComboBox<String> condaEnvCombo;
    private JRadioButton condaRadioButton, customPythonRadioButton;
    private IcyTextField customPythonPathText;
    private Set<String> condaEnvList;

    public JepPreferencesWindow() {
        super("JEP configuration", true, true, true, true);

        // UI
        initialize();

        // Listeners
        condaRadioButton.addActionListener(this);
        customPythonRadioButton.addActionListener(this);
        customPythonBrowseButton.addActionListener(this);
        applyButton.addActionListener(this);
        saveButton.addActionListener(this);
        cancelButton.addActionListener(this);

        // Update UI with saved settings
        validate();
        load();
    }

    private void initialize() {
        JPanel mainPanel = new JPanel(createGridLayout());
        mainPanel.setLayout(createGridLayout());

        condaEnvList = PythonUtils.getInstance().getCondaEnvNames();

        GridBagConstraints condaCons = createConstraints(0, 2, 2., GridBagConstraints.FIRST_LINE_START, new int[]{15, 0, 8, 5});

        // First choice -> use a Conda environment
        condaRadioButton = createRadioButton("Use a Conda (Anaconda, Miniconda, Mamba, etc...) environment");
        mainPanel.add(condaRadioButton, condaCons);

        if (condaEnvList != null) {
            condaEnvCombo = createComboBox(condaEnvList.toArray(new String[0]));
        }
        else {
            condaRadioButton.setEnabled(false);
            condaEnvCombo = createComboBox(new String[]{});
        }

        condaEnvCombo.setEnabled(false);
        condaCons.gridx = 2;
        condaCons.weightx = 0.;
        condaCons.gridwidth = 1;
        condaCons.fill = GridBagConstraints.NONE;
        condaCons.insets = new Insets(15, 0, 0, 0);
        mainPanel.add(condaEnvCombo, condaCons);

        // Second choice -> use another Python installed
        customPythonRadioButton = createRadioButton("Set manually the path to the Python home directory");
        mainPanel.add(customPythonRadioButton, createConstraints(1, 1, 1., GridBagConstraints.FIRST_LINE_START, new int[]{0, 0, 8, 0}));

        GridBagConstraints customPythonConstraints = createConstraints(2, 2, 2., GridBagConstraints.FIRST_LINE_START, new int[]{0, 0, 0, 5});

        customPythonPathText = createTextInput();
        customPythonPathText.setEnabled(false);
        mainPanel.add(customPythonPathText, customPythonConstraints);

        customPythonBrowseButton = createButton("Browse");
        customPythonBrowseButton.setEnabled(false);
        customPythonConstraints.gridx = 2;
        customPythonConstraints.gridwidth = 1;
        customPythonConstraints.weightx = 1.;
        customPythonConstraints.weighty = 1.;
        customPythonConstraints.fill = GridBagConstraints.NONE;
        mainPanel.add(customPythonBrowseButton, customPythonConstraints);

        // Group radio buttons for selection
        ButtonGroup radioGroup = createRadioGroup();
        radioGroup.add(condaRadioButton);
        radioGroup.add(customPythonRadioButton);

        if (condaEnvList == null) {
            JLabel condaNotInstalled = new JLabel("Conda is currently not installed");
            condaNotInstalled.setFont(new Font(condaNotInstalled.getFont().getName(), Font.ITALIC, condaNotInstalled.getFont().getSize()));

            mainPanel.add(condaNotInstalled, createConstraints(3, 2, 1., GridBagConstraints.CENTER, new int[]{0, 0, 0, 0}));
        }

        // State buttons
        Box buttonsBox = Box.createHorizontalBox();
        GridBagConstraints buttonsConstraints = createConstraints(condaEnvList != null ? 3 : 4, 1, 0., GridBagConstraints.LAST_LINE_END, new int[]{0, 5, 5, 5});
        buttonsConstraints.fill = GridBagConstraints.NONE;

        applyButton = createButton("Apply");
        buttonsBox.add(applyButton);

        buttonsBox.add(Box.createHorizontalStrut(5));

        saveButton = createButton("Ok");
        buttonsConstraints.gridx++;
        buttonsBox.add(saveButton);

        buttonsBox.add(Box.createHorizontalStrut(5));

        cancelButton = createButton("Cancel");
        buttonsConstraints.gridx++;
        buttonsBox.add(cancelButton);

        mainPanel.add(buttonsBox, buttonsConstraints);

        setContentPane(mainPanel);
    }

    private void load() {
        String pythonHomePath = JepPreferences.getInstance().getPythonRoot();

        if (!pythonHomePath.isEmpty()) {
            if (pythonHomePath.contains("conda")) {
                condaRadioButton.setSelected(true);
                condaEnvCombo.setEnabled(true);

                for (String envName : PythonUtils.getInstance().getCondaEnvNames()) {
                    Pattern pattern = Pattern.compile(envName);
                    Matcher matcher = pattern.matcher(pythonHomePath);

                    if (matcher.find()) {
                        condaEnvCombo.setSelectedItem(envName);
                    }
                }
            }
            else {
                customPythonRadioButton.setSelected(true);
                customPythonPathText.setEnabled(true);
                customPythonPathText.setText(pythonHomePath);
                customPythonBrowseButton.setEnabled(true);
            }
        }
    }

    private boolean apply() {
        String pythonHomePath = "";

        if (condaRadioButton.isSelected()) {
            pythonHomePath = PythonUtils.getInstance().detectCondaEnvironments().get((String) condaEnvCombo.getSelectedItem());
        }
        if (customPythonRadioButton.isSelected()) {
            pythonHomePath = customPythonPathText.getText();
        }

        String pythonExecutablePath = PythonUtils.getInstance().findPythonExecutable(pythonHomePath, true);

        if (pythonExecutablePath != null) {
            JepPreferences.getInstance().setPythonRoot(pythonHomePath);
            JepPreferences.getInstance().setPythonExecPath(pythonExecutablePath);

            String sitePackageDirectory = PythonUtils.getInstance().setSitePackagesDirectory();
            String jepPath = JepUtils.getInstance().findJepLib(sitePackageDirectory);

            if (jepPath == null) {
                PythonUtils.getInstance().installPythonPackage("wheel numpy jep");
                jepPath = JepUtils.getInstance().findJepLib(sitePackageDirectory);
            }

            JepPreferences.getInstance().setJepPath(jepPath);
        }
        else {
            MessageDialog.showDialog("Error while finding Python", "Python could not be found. Please select an other directory.", MessageDialog.ERROR_MESSAGE);

            return false;
        }

        MessageDialog.showDialog("JEP configuration saved", "JEP settings has been successfully saved", MessageDialog.INFORMATION_MESSAGE);

        return true;
    }

    private void save() {
        if (apply()) {
            frame.dispose();
        }
    }

    private void cancel() {
        frame.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource().equals(condaRadioButton)) {
            if (condaEnvList != null) {
                condaEnvCombo.setEnabled(condaRadioButton.isSelected());
            }
            else {
                condaEnvCombo.setEnabled(false);
            }

            customPythonPathText.setEnabled(false);
            customPythonBrowseButton.setEnabled(false);
        }
        else if (actionEvent.getSource().equals(customPythonRadioButton)) {
            if (customPythonRadioButton.isSelected()) {
                condaEnvCombo.setEnabled(false);
                customPythonPathText.setEnabled(true);
                customPythonBrowseButton.setEnabled(true);
            }
            else {
                condaEnvCombo.setEnabled(false);
                customPythonPathText.setEnabled(false);
                customPythonBrowseButton.setEnabled(false);
            }
        }
        else if (actionEvent.getSource().equals(customPythonBrowseButton)) {
            createDirectorySelection();
        }
        else if (actionEvent.getSource().equals(applyButton)) {
            apply();
        }
        else if (actionEvent.getSource().equals(saveButton)) {
            save();
        }
        else if (actionEvent.getSource().equals(cancelButton)) {
            cancel();
        }
    }

    /**
     * Delegate function to create a GridBagLayout
     *
     * @return New instance of GridBagLayout
     */
    private GridBagLayout createGridLayout() {
        return new GridBagLayout();
    }

    /**
     * Delegate function to add constraints for a grid cell
     *
     * @param y      Position in vertical axis of the grid
     * @param width  Width of the cell in the grid
     * @param anchor Position the cell in a specific location of the grid
     * @param insets Add margins to the cell with Insets class (top, left, bottom right)
     * @return New instance of GridBagConstraints
     */
    private GridBagConstraints createConstraints(int y, int width, double weightX, int anchor, int[] insets) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.gridwidth = width;
        constraints.gridheight = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = anchor;
        constraints.insets = new Insets(insets[0], insets[1], insets[2], insets[3]);
        constraints.weightx = weightX;
        constraints.weighty = 0.0;

        return constraints;
    }

    /**
     * Delegate function to add a ButtonGroup
     *
     * @return New instance of ButtonGroup
     */
    private ButtonGroup createRadioGroup() {
        return new ButtonGroup();
    }

    /**
     * Delegate function to create a JRadioButton
     *
     * @param text Text to add in radio button
     * @return New instance of JRadioButton
     */
    private JRadioButton createRadioButton(String text) {
        return new JRadioButton(text);
    }

    /**
     * Delegate function to create a new JRadioButton
     *
     * @return New instance of JButton
     */
    private JButton createButton(String text) {
        return new JButton(text);
    }

    /**
     * Delegate function to create a new JComboBox with different kind of data
     *
     * @param content Array of element to list
     * @param <T>     Generic type
     * @return New instance of JComboBox
     */
    private <T> JComboBox<T> createComboBox(T[] content) {
        return new JComboBox<>(content);
    }

    /**
     * Delegate function to create a new IcyTextField (JTextField)
     *
     * @return New instance of IcyTextField
     */
    private IcyTextField createTextInput() {
        return new IcyTextField("");
    }

    /**
     * Delegate function to manage JFileChooser
     */
    private void createDirectorySelection() {
        String pythonPathInput = customPythonPathText.getText();

        JFileChooser selectPythonDirectoryDiag = new JFileChooser();
        selectPythonDirectoryDiag.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        selectPythonDirectoryDiag.setAcceptAllFileFilterUsed(false);
        selectPythonDirectoryDiag.setDialogTitle("Set Python home directory");

        if (!pythonPathInput.isEmpty()) {
            Path pythonHomePath = Paths.get(pythonPathInput);
            boolean isDirectory = pythonHomePath.toFile().isDirectory();

            selectPythonDirectoryDiag.setCurrentDirectory(isDirectory ? pythonHomePath.toFile() : pythonHomePath.getParent().toFile());
        }

        if (selectPythonDirectoryDiag.showOpenDialog(customPythonBrowseButton) == JFileChooser.APPROVE_OPTION) {
            customPythonPathText.setText(selectPythonDirectoryDiag.getSelectedFile().getAbsolutePath());
        }
    }
}
