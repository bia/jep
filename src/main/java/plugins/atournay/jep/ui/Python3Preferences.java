package plugins.atournay.jep.ui;

import icy.gui.frame.IcyFrameAdapter;
import icy.gui.frame.IcyFrameEvent;
import icy.plugin.abstract_.PluginActionable;
import icy.plugin.interface_.PluginBundled;
import plugins.atournay.jep.JepPlugin;

public class Python3Preferences extends PluginActionable implements PluginBundled {
    @Override
    public void run() {
        final JepPreferencesWindow frameWindow = new JepPreferencesWindow();
        frameWindow.setSize(700, 180);
        frameWindow.setResizable(false);
        frameWindow.setVisible(true);
        frameWindow.addToDesktopPane();
        frameWindow.requestFocus();

        frameWindow.addFrameListener(new IcyFrameAdapter() {
            @Override
            public void icyFrameClosed(IcyFrameEvent icyFrameEvent) {
                frameWindow.removeFrameListener(this);
            }
        });
    }

    @Override
    public String getMainPluginClassName() {
        return JepPlugin.class.getName();
    }
}
