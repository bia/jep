package plugins.atournay.jep.utils;

import jep.*;
import plugins.atournay.jep.exec.DefaultClassEnquirer;

import java.io.File;

/**
 * Utility functions to configure JEP
 *
 * @author Amandine Tournay
 */
public class JepUtils {
    private static JepUtils instance;
    private JepConfig jepConfig;

    private JepUtils() {
    }

    public static JepUtils getInstance() {
        if (instance == null) {
            instance = new JepUtils();
        }

        return instance;
    }

    /**
     * @param sitePackagesPath Path to the site-packages directory
     * @return Path to JEP
     */
    public String findJepLib(String sitePackagesPath) {
        String jetPath = "";
        File jepDir = new File(sitePackagesPath, "jep");

        if (!jepDir.isDirectory()) {
            return null;
        }
        else {
            String[] libFiles = {"libjep.so", "libjep.jnilib", "jep.ddl"};

            for (String libFile : libFiles) {
                File lib = new File(jepDir, libFile);

                if (lib.isFile()) jetPath = lib.getAbsolutePath();
            }
        }

        return jetPath;
    }

    /**
     * Set the path of the JEP library either the OS
     *
     * @param jepPath    Path the JEP execution file (libjep.(so|jnilib|ddl))
     * @param pythonRoot Path to the Python root directory
     */
    public void setJepPath(String jepPath, String pythonRoot) throws JepException {
        setJepConfig(new JepConfig());

        getJepConfig().setClassEnquirer(new DefaultClassEnquirer());

        if (jepPath.matches(".*conda.*|.*venv")) {
            PyConfig pyConfig = new PyConfig();
            pyConfig.setPythonHome(pythonRoot);

            MainInterpreter.setInitParams(pyConfig);
        }

        SharedInterpreter.setConfig(getJepConfig());
        MainInterpreter.setJepLibraryPath(jepPath);
    }

    /**
     * Create a new Python instance
     *
     * @return Python interpreter to execute some Python commands, retrieve values, etc...
     */
    public SubInterpreter openSubPython() {
        return openSubPython(new JepConfig());
    }

    /**
     * Create a new Python instance
     *
     * @param jepConfig JEP configurations {@link  jep.JepConfig} and example at {@link DefaultClassEnquirer}
     * @return Python interpreter to execute some Python commands, retrieve values, etc...
     */
    public SubInterpreter openSubPython(JepConfig jepConfig) {
        return new SubInterpreter(jepConfig);
    }

    /**
     * Close a running Python sub interpreter
     *
     * @param subInterpreter Running Python instance
     */
    public void closeSubPython(SubInterpreter subInterpreter) {
        subInterpreter.close();
    }

    // GETTERS - SETTERS

    public JepConfig getJepConfig() {
        return jepConfig;
    }

    private void setJepConfig(JepConfig newJepConfig) {
        this.jepConfig = newJepConfig;
    }
}
