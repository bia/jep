package plugins.atournay.jep.utils;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.system.IcyExceptionHandler;
import icy.system.thread.ThreadUtil;
import plugins.atournay.jep.prefs.JepPreferences;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PythonUtils {
    private static PythonUtils instance;

    private PythonUtils() {
    }

    public static PythonUtils getInstance() {
        if (instance == null) {
            instance = new PythonUtils();
        }

        return instance;
    }

    /**
     * List all Conda environments available on the computers
     *
     * @return List of conda environments (Key = Name of environment; Value = Path of the environment)
     */
    public HashMap<String, String> detectCondaEnvironments() {
        HashMap<String, String> condaEnvs = new HashMap<>();
        String[] cmd = new String[]{"conda", "env", "list"};

        try {
            Process process = new ProcessBuilder(cmd).start();

            BufferedReader stdOut = new BufferedReader(new InputStreamReader(process.getInputStream()));

            Pattern pattern = Pattern.compile("#|([^.A-z]conda)|(environments:)|\\*| ");

            List<String> stdOutStr = stdOut.lines().flatMap(pattern::splitAsStream)
                    .collect(Collectors.toList())
                    .stream().filter(e -> !e.equals("")).collect(Collectors.toList());

            for (int i = 0; i < stdOutStr.size(); i += 2) {
                condaEnvs.put(stdOutStr.get(i), stdOutStr.get(i + 1));
            }
        }
        catch (IOException e) {
            return null;
        }

        return condaEnvs;
    }

    /**
     * Get Conda environment names available on the computer
     *
     * @return List of all Conda environment names
     */
    public Set<String> getCondaEnvNames() {
        return detectCondaEnvironments() != null ? detectCondaEnvironments().keySet() : null;
    }

    /**
     * The Python executable file in Windows is python.exe and in Unix systems /python (which can be linked to a specific version file)
     *
     * @param path        The path to a directory where the Python executable must be found
     * @param isDirectory Set if the path is ending by a directory (true) or a file (false)
     * @return The full path of the Python execution file
     */
    public String findPythonExecutable(String path, boolean isDirectory) {
        if (isDirectory) {
            File pathToTest = new File(path);
            File[] pythonFiles = Utils.getInstance().isWindows() ? pathToTest.listFiles(File::isFile) : pathToTest.listFiles((file, name) -> name.equals("bin") && file.isDirectory());

            if (pythonFiles != null) {
                if (Utils.getInstance().isWindows()) {
                    File foundPython = Arrays.stream(pythonFiles)
                            .filter(x -> Pattern.compile("python?3?(?:\\.exe|$)").matcher(x.getAbsolutePath()).find())
                            .findFirst().orElse(null);

                    return foundPython != null ? foundPython.getAbsolutePath() : null;
                }
                else {
                    for (File binDirectory : pythonFiles) {
                        File[] binFiles = binDirectory.listFiles((file, name) -> name.matches("python?3?(?:\\.exe|$)"));

                        if (binFiles != null) {
                            return Arrays.stream(binFiles).findFirst().map(File::getAbsolutePath).orElse(null);
                        }
                    }
                }
            }
        }
        else {
            if (Files.exists(Paths.get(path))) {
                return path;
            }
            else {
                return null;
            }
        }

        return null;
    }

    /**
     * @return The path to the site-packages directory
     */
    public String setSitePackagesDirectory() {
        String terminalLine;
        String[] command = new String[]{JepPreferences.getInstance().getPythonExecPath(), "-c", "import sys; print([p for p in sys.path if p.endswith(\"site-packages\")])"};
        String sitePackagesPath = "";

        try {
            // Execute terminal command to find the site-packages directory
            Process process = Runtime.getRuntime().exec(command);

            BufferedReader streamInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader streamError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // Execution result stream output
            while ((terminalLine = streamInput.readLine()) != null) {
                String[] spList = terminalLine.replaceAll("[\\[\\]' ]", "").split(",");

                for (String spPath : spList) {
                    if (spPath.contains("site-packages")) {
                        sitePackagesPath = spPath;
                        break;
                    }
                }
            }

            // Error stream output
            while ((terminalLine = streamError.readLine()) != null) {
                System.err.println(terminalLine);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return sitePackagesPath;
    }

    /**
     * Install Python packages through pip via the terminal of the computer
     * @see plugins.atournay.jep.exec.PythonExec#installPythonPackage(String) to install a Python package through Python
     *
     * @param packageNames Name of the packages to install through pip.
     *                     It can be either the package name (i.e. numpy) or a specific version of a package (i.e. numpy==1.23.1) separated by a space
     */
    public void installPythonPackage(String packageNames) {
        // Create command to execute (i.e. /path/to/python -m pip install package1 package2 ...)
        String pythonExec = Utils.getInstance().isWindows() ? JepPreferences.getInstance().getPythonExecPath().replace("\\", "\\\\") : JepPreferences.getInstance().getPythonExecPath();
        ArrayList<String> packageList = Arrays.stream(packageNames.trim().split(" ")).filter(s -> !s.equals("")).map(String::trim).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<String> commandLine = new ArrayList<>();
        commandLine.add(pythonExec);
        commandLine.add("-m");
        commandLine.add("pip");
        commandLine.add("install");
        commandLine.addAll(packageList);

        // Create window to get output console
        final IcyFrame[] installFrame = new IcyFrame[1];
        JTextArea outputArea = new JTextArea();

        ThreadUtil.invokeNow(() -> {
            installFrame[0] = new IcyFrame("Installing Python packages " + packageNames, false, false);
            installFrame[0].getContentPane().setLayout(new GridLayout());
            installFrame[0].setSize(1000, 300);
            installFrame[0].setVisible(true);
            installFrame[0].addToDesktopPane();
            installFrame[0].requestFocus();

            installFrame[0].getContentPane().add(outputArea);
        });

        ThreadUtil.bgRun(() -> {
            // Execute installation with pip
            ProcessBuilder command = new ProcessBuilder(commandLine);

            // Retrieve output
            Process sysProc;
            BufferedReader bufferedReader;
            BufferedReader bufferedErrReader;
            try {
                sysProc = command.start();
                bufferedReader = new BufferedReader(new InputStreamReader(sysProc.getInputStream()));
                bufferedErrReader = new BufferedReader(new InputStreamReader(sysProc.getErrorStream()));
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }

            try {
                String line, lineError;

                // Update window with new output element
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);

                    String finalLine = line;
                    ThreadUtil.invokeLater(() -> {
                        outputArea.append(finalLine);
                        outputArea.append("\n");
                        installFrame[0].validate();
                    });
                }

                while ((lineError = bufferedErrReader.readLine()) != null) {
                    System.out.println(lineError);

                    String finalLineError = lineError;
                    ThreadUtil.invokeLater(() -> {
                        outputArea.append(finalLineError);
                        outputArea.append("");
                        installFrame[0].validate();
                    });
                }

                sysProc.waitFor();

                Thread.sleep(2000);

                // Close window
                ThreadUtil.invokeLater(() -> installFrame[0].dispose());
            }
            catch (IOException | InterruptedException e) {
                MessageDialog.showDialog("Error while trying to install " + packageNames, e.toString(), MessageDialog.ERROR_MESSAGE);

                IcyExceptionHandler.showErrorMessage(new RuntimeException(e), true);
            }
        });
    }
}
