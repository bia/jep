package plugins.atournay.jep.utils;

import java.util.regex.Pattern;

public class Utils {
    private static Utils instance;

    private Utils() {}

    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }

        return instance;
    }

    /**
     * @param path The path to test
     * @return The result if the String is really a path (compatible Windows, MacOS &amp; Linux)
     */
    public boolean isPath(String path) {
        if (Pattern.compile("([A-Za-z]:|[/\\\\])+.(\\w)+.*").matcher(path).find()) {
            return true;
        }
        else {
            System.err.println("The input does not correspond to a System path. Please retry.");
            System.out.println();
            return false;
        }
    }

    public String getOS() {
        return System.getProperty("os.name").toLowerCase();
    }

    public boolean isWindows() {
        return getOS().contains("win");
    }

    public boolean isMacOS() {
        return getOS().startsWith("mac");
    }

    public boolean isLinux() {
        return getOS().startsWith("nix") || getOS().contains("nux") || getOS().contains("aix");
    }
}
